package goiania.allvara;

import goiania.allvara.service.BombeiroService;
import goiania.allvara.service.GovernmentService;
import goiania.allvara.service.TesseractService;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class ServiceApplication {

    @Autowired
    private TesseractService tesseractService;

    @Autowired
    private GovernmentService service;

    @Autowired
    private BombeiroService bombeiroService;

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(ServiceApplication.class, args);
    }

    @PostConstruct
    public void teste(){

        //bombeiroService.teste();

        try {
//
//            byte[] content = Files.readAllBytes(new File("/Users/user/tesseract/doc_20190607_5.jpg").toPath());
//            var ret = tesseractService.read("bombeiro.jpg",content);
//            var map = new HashMap<String,String>();
//            if(!ObjectUtils.isEmpty(ret)){
//                String protocol = "";
//                String reason = "";
//                String cnpj = "";
//                String cercon = "";
//                var lines = ret.split("\n");
//                for (int i = 0; i < lines.length ; i++) {
//                    if(lines[i].contains("CERTIFICADO DE CONFORMIDADE")){
//                        protocol = lines[i].replace("CERTIFICADO DE CONFORMIDADE","").trim();
//                    }else if(lines[i].contains("Razao Social CNPJ/CPF")){
//                        reason = lines[i +2].split("CNPJ:")[0];
//                        cnpj = lines[i +2].split("CNPJ:")[1];
//                    }else if(lines[i].contains("Codigo de controle do CERCON:")){
//                        cercon = lines[i].replace("Codigo de controle do CERCON:","").trim();
//                    }
//                }
//                System.out.println(protocol);
//            }
            //service.findCompany("24081684000161");
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
//"27865757000102"
    @Bean
    public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();
        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate;
    }


}