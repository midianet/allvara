package goiania.allvara.repository;

import goiania.allvara.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company,Long>{

    Optional<Company> findByCnpj(String cnpj);

}