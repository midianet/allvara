package goiania.allvara.repository;

import goiania.allvara.domain.Process;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessRepository extends JpaRepository<Process,Long>{

}