package goiania.allvara.repository;

import goiania.allvara.domain.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document,Long>{

}