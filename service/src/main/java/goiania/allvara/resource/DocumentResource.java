package goiania.allvara.resource;

import goiania.allvara.domain.Document;
import goiania.allvara.repository.DocumentRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/documents")
public class DocumentResource {

    @Autowired
    private DocumentRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public Document post(@RequestBody Document document, HttpServletResponse response) {
        log.trace("POST {document}", document);
        document.setId(null);
        document = repository.save(document);
        response.addHeader(HttpHeaders.LOCATION, ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(document.getId())
                .toUri().toString() + document.getId());
        return document;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void put(@PathVariable Long id, @RequestBody Document document) {
        log.trace("PUT document {}", document);
        var persistent = findById(id);
        BeanUtils.copyProperties(document, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        log.trace("DELETE {id}", id);
        findById(id);
        repository.deleteById(id);
    }

    @GetMapping
    public List<Document> list() {
        log.trace("GET Document");
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Document findById(@PathVariable Long id) {
        log.trace("GET Document {id}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("document %d", id)));
    }

    @GetMapping(path = "/states")
    public List<Document.State> listStates() {
        log.trace("GET Document States");
        return Arrays.asList(Document.State.values());
    }

}