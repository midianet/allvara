package goiania.allvara.resource;

import goiania.allvara.domain.Process;
import goiania.allvara.repository.ProcessRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/process")
public class ProcessResource {

    @Autowired
    private ProcessRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public Process post(@RequestBody Process process, HttpServletResponse response) {
        log.trace("POST {process}", process);
        process.setId(null);
        process = repository.save(process);
        response.addHeader(HttpHeaders.LOCATION, ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(process.getId())
                .toUri().toString() + process.getId());
        return process;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void put(@PathVariable Long id, @RequestBody Process process) {
        log.trace("PUT process {}", process);
        var persistent = findById(id);
        BeanUtils.copyProperties(process, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        log.trace("DELETE {id}", id);
        findById(id);
        repository.deleteById(id);
    }

    @GetMapping
    public List<Process> list() {
        log.trace("GET Process");
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Process findById(@PathVariable Long id) {
        log.trace("GET Process {id}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("process %d", id)));
    }

    @GetMapping(path = "/types")
    public List<Process.Type> listTypes() {
        log.trace("GET Process Types");
        return Arrays.asList(Process.Type.values());
    }

    @GetMapping(path = "/states")
    public List<Process.State> listStates() {
        log.trace("GET Process States");
        return Arrays.asList(Process.State.values());
    }

}