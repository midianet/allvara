package goiania.allvara.resource;

import goiania.allvara.domain.Company;
import goiania.allvara.repository.CompanyRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/companies")
public class CompanyResource {

    @Autowired
    private CompanyRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public Company post(@RequestBody Company company, HttpServletResponse response) {
        log.trace("POST {company}", company);
        company.setId(null);
        company = repository.save(company);
        response.addHeader(HttpHeaders.LOCATION, ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(company.getId())
                .toUri().toString() + company.getId());
        return company;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void put(@PathVariable Long id, @RequestBody Company company) {
        log.trace("PUT company {}", company);
        var persistent = findById(id);
        BeanUtils.copyProperties(company, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        log.trace("DELETE {id}", id);
        findById(id);
        repository.deleteById(id);
    }

    @GetMapping
    public List<Company> list() {
        log.trace("GET Company");
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Company findById(@PathVariable Long id) {
        log.trace("GET Company {id}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Company %d", id)));
    }

}