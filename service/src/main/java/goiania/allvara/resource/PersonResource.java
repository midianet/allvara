package goiania.allvara.resource;

import goiania.allvara.domain.Person;
import goiania.allvara.repository.PersonRepository;
import goiania.allvara.security.PersonSystem;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/persons")
public class PersonResource {

    @Autowired
    private PersonRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public Person post(@RequestBody Person person, HttpServletResponse response) {
        log.trace("POST {person}", person);
        person.setId(null);
        person = repository.save(person);
        response.addHeader(HttpHeaders.LOCATION, ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(person.getId())
                .toUri().toString() + person.getId());
        return person;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void put(@PathVariable Long id, @RequestBody Person person) {
        log.trace("PUT person {}", person);
        var persistent = findById(id);
        BeanUtils.copyProperties(person, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        log.trace("DELETE {id}", id);
        findById(id);
        repository.deleteById(id);
    }

    @GetMapping
    public List<Person> list() {
        log.trace("GET Person");
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Person findById(@PathVariable Long id) {
        log.trace("GET Person {id}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("person %d", id)));
    }

    @GetMapping(path = "/types")
    public List<Person.Type> listTypes() {
        log.trace("GET Person Types");
        return Arrays.asList(Person.Type.values());
    }

    @GetMapping(path = "/me")
    public Person me(){
        var principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ((PersonSystem) principal).getUser();
    }

}