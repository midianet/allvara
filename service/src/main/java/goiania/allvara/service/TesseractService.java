package goiania.allvara.service;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Files;

@Service
public class TesseractService {

    public String read(String name, byte[] content){
        try {
            Files.write(new File("/Users/user/tesseract/" + name).toPath(), content);
            var pb = new ProcessBuilder();
            pb.command("bash", "-c", "docker run -v /Users/user/tesseract:/tmp --rm jitesoft/tesseract-ocr /tmp/" + name + " stdout");
            var process = pb.start();
            var output = new StringBuilder();
            var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                return output.toString();
            } else {
                throw new RuntimeException("Error read..");
            }
        }catch (Exception e ){
            throw new RuntimeException(e);
        }
    }

}
