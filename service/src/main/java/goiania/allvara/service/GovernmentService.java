package goiania.allvara.service;

import goiania.allvara.api.government.Establishment;
import goiania.allvara.domain.Company;
import goiania.allvara.repository.CompanyRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GovernmentService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private  RestTemplate restTemplate;

    public Company findCompany(String cnpj){
        var ret = restTemplate.getForObject("https://www.receitaws.com.br/v1/cnpj/" + cnpj, Establishment.class);
        if("ERROR".equals(ret.getStatus())){
            throw new RuntimeException(ret.getMessage());
        }
        var company = ret.toCompany();
        var persistent = companyRepository.findByCnpj(cnpj);
        if(persistent.isPresent()){
            BeanUtils.copyProperties(company,persistent,"id");
            companyRepository.save(persistent.get());
            return persistent.get();
        }else{
            company = companyRepository.save(company);
            return company;
        }
    }

}