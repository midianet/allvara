package goiania.allvara.security;

import goiania.allvara.domain.Person;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class PersonSystem extends User {

    @Getter
    private Person user;

    public PersonSystem(Person person, Collection<? extends GrantedAuthority> authorities) {
        super(person.getEmail(), person.getPassword(), authorities);
        this.user = person;
    }

}