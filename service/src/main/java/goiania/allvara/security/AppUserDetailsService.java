package goiania.allvara.security;

import goiania.allvara.domain.Person;
import goiania.allvara.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;

@Service
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    private PersonRepository repository;

    @Override
    public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
        var person = repository.findByCpf(cpf).orElseThrow(() -> new UsernameNotFoundException("Cpf e/ou senha incorretos"));
        return new PersonSystem(person, getPermissoes(person));
    }

    private Collection<? extends GrantedAuthority> getPermissoes(Person person) {
        var authorities = new HashSet<SimpleGrantedAuthority>();
        if(person.getType().equals(Person.Type.CLIENT.getDef())){
            authorities.add(new SimpleGrantedAuthority("CLIENT"));
        }else{
            authorities.add(new SimpleGrantedAuthority("SERVER"));
        }
        return authorities;
    }

}