
package goiania.allvara.api.government;

import goiania.allvara.domain.Company;
import lombok.Data;

@Data
public class Establishment {

    private String email;
    private String nome;
    private String cnpj;
    private String tipo;
    private String situacao;
    private String fantasia;
    private String telefone;
    private String capitalSocial;
    private String porte;
    private String cep;
    private String bairro;
    private String logradouro;
    private String municipio;
    private String uf;
    private String numero;
    private String naturezaJuridica;
    private String status;
    private String message;

    public Company toCompany(){
        return Company
                .builder()
                .email(email)
                .cnpj(cnpj.replace(".","").replace("-","").replace("/",""))
                .reason(nome)
                .type(tipo)
                .situation(situacao)
                .fantasy(fantasia)
                .phone(telefone)
                .port(porte)
                .cep(cep.replace(".","").replace("-",""))
                .neighborhood(bairro)
                .place(logradouro)
                .city(municipio)
                .state(uf)
                .number(numero)
                .nature(naturezaJuridica)
                .build();
    }

}
