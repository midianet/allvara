package goiania.allvara.domain;

import lombok.*;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 100,nullable = false)
    private String reason;

    @CNPJ
    @NotNull
    @Column(length = 14,nullable = false)
    private String cnpj;

    @Column(length = 80)
    private String email;

    @Column(length = 20)
    private String type;

    @Column(length = 20)
    private String situation;

    @Column(length = 100)
    private String fantasy;

    @Column(length = 50)
    private String phone;

    @Column(length =  20)
    private String port;

    @Column(length = 10)
    private String cep;

    @Column(length = 80)
    private String neighborhood;

    @Column(length = 150)
    private String place;

    @Column(length = 80)
    private String city;

    @Column(length = 2)
    private String state;

    @Column(length = 20)
    private String number;

    @Column(length = 20)
    private String nature;
}