package goiania.allvara.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Param {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(length = 50,nullable = false)
    private String key;

    @NotNull
    @Column(length = 255,nullable = false)
    private String value;

    @ManyToOne
    private Document document;

}
