package goiania.allvara.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Process {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    @Column(nullable = false)
    private LocalDate date;

    @NotNull
    @ManyToOne
    private Person  person;

    @NotNull
    @ManyToOne
    private Company company;

    @NotNull
    @Column(length = 1, nullable = false)
    private State state;
    @NonNull
    @Column(length = 2, nullable = false)
    private Type type;

    @OneToMany
    private List<Document> documents;

    @AllArgsConstructor
    public enum Type {
        ALVARA_LOCALIZACAO("AL"), ALVARA_HORARIO("AH"), ALVARA_CALCADA( "AC");

        @Getter
        private String def;

        public static Type toEnum(String def){
            switch (def){
                case "AL" :
                    return Type.ALVARA_LOCALIZACAO;
                case "AH" :
                    return Type.ALVARA_HORARIO;
                case "AC" :
                    return Type.ALVARA_CALCADA;
                default:
                    throw new IllegalArgumentException("Invalid value Type");
            }
        }

    }

    @AllArgsConstructor
    public enum State {
        INIIIAL("I"), DELIVERY("D"), PENDING("P") , ANALIZED( "A");

        @Getter
        private String def;

        public static State toEnum(String def){
            switch (def){
                case "I" :
                    return State.INIIIAL;
                case "D" :
                    return State.DELIVERY;
                case "P" :
                    return State.PENDING;
                case "O" :
                    return State.ANALIZED;
                default:
                    throw new IllegalArgumentException("Invalid value State");
            }
        }

    }

}