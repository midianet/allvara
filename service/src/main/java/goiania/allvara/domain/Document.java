package goiania.allvara.domain;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private LocalDate date;

    @NotNull
    @Column(nullable = false, length = 255)
    private String url;

    @NotNull
    @Column(length = 255)
    private String obs;

    @NotNull
    @Column(length = 1)
    private State state;

    @NotNull
    @Column(length = 1)
    private Document.Type type;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Param> params = new ArrayList<>();

    @AllArgsConstructor
    public enum State {
        RECEIVED("R"), PENDING("P") ,  DENIED("D") ,  ANALIZED( "O");

        @Getter
        private String def;

        public static State toEnum(String def){
            switch (def){
                case "R" :
                    return State.RECEIVED;
                case "P" :
                    return State.PENDING;
                case "D" :
                    return State.DENIED;
                case "A" :
                    return State.ANALIZED;
                default:
                    throw new IllegalArgumentException("Invalid value State");
            }
        }

    }



    @AllArgsConstructor
    public enum Type {
        BOMBEIRO("B"), USO_SOLO("S"), NUMERO_OFICIAL( "N");

        @Getter
        private String def;

        public static Type toEnum(String def){
            switch (def){
                case "B" :
                    return Type.BOMBEIRO;
                case "S" :
                    return Type.USO_SOLO;
                case "N" :
                    return Type.NUMERO_OFICIAL;
                default:
                    throw new IllegalArgumentException("Invalid value Type");
            }
        }

    }


}