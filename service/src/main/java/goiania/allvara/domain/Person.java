package goiania.allvara.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 80,nullable = false)
    private String name;

    @NotNull
    @Column(length = 20,nullable = false)
    private String password;

    @NotNull
    @Column(length = 11,nullable = false)
    private String cpf;

    private Long telegram;

    @NotNull
    @Column(length = 15,nullable = false)
    private String phone;

    @NotNull
    @Column(length = 40,nullable = false)
    private String email;

    @NotNull
    @Column(length = 80,nullable = false)
    private String type;

    @AllArgsConstructor
    public enum Type {
        CLIENT("C"), SERVER("S");

        @Getter
        private String def;

        public static Type toEnum(String def){
            switch (def){
                case "C" :
                    return Type.CLIENT;
                case "S" :
                    return Type.SERVER;
                default:
                    throw new IllegalArgumentException("Invalid value Type");
            }
        }

    }


}
