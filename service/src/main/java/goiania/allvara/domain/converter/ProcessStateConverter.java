package goiania.allvara.domain.converter;

import goiania.allvara.domain.Process;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ProcessStateConverter implements AttributeConverter<Process.State , String> {

    @Override
    public String convertToDatabaseColumn(Process.State type) {
        return type.getDef();
    }

    @Override
    public Process.State convertToEntityAttribute(String def) {
        return Process.State.toEnum(def);
    }
}
