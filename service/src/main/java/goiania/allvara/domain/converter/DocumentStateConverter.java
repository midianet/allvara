package goiania.allvara.domain.converter;

import goiania.allvara.domain.Document;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DocumentStateConverter implements AttributeConverter<Document.State , String> {

    @Override
    public String convertToDatabaseColumn(Document.State type) {
        return type.getDef();
    }

    @Override
    public Document.State convertToEntityAttribute(String def) {
        return Document.State.toEnum(def);
    }

}