package goiania.allvara.domain.converter;

import goiania.allvara.domain.Person;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PersonTypeConverter implements AttributeConverter<Person.Type, String> {

    @Override
    public String convertToDatabaseColumn(Person.Type type) {
        return type.getDef();
    }

    @Override
    public Person.Type convertToEntityAttribute(String def) {
        return Person.Type.toEnum(def);
    }



}