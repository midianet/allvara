package goiania.allvara.domain.converter;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import goiania.allvara.domain.Process;

@Converter(autoApply = true)
public class ProcessTypeConverter implements AttributeConverter<Process.Type , String> {

    @Override
    public String convertToDatabaseColumn(Process.Type type) {
        return type.getDef();
    }

    @Override
    public Process.Type convertToEntityAttribute(String def) {
        return Process.Type.toEnum(def);
    }

}