app.controller('AlvaraFormController', ['$scope','$http','$location',

    function ($scope,$http,$location) {
        $scope.save = function (){
            if(!$scope.frm.$invalid) {
                $location.path('alvara-confirmed');
            }
        };

        $scope.validadeForm = function(){
            var element = document.getElementById("frm");
            element.classList.add("was-validated");
        }
    }
]);