app.controller('MainController', ['$scope','$http',
    function ($scope,$http) {

        $scope.user = {};

        console.log($http.defaults.headers);
        $http.get('http://localhost:8080/api/persons/me')
            .success(function (user) {
                $scope.user = user;
            });

    }
]);